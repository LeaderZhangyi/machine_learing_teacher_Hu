# -*- coding: utf-8 -*-
"""
《《《多元线性回归》》》
   商品销售额预测：与电视广告投入、收音机广告投入、报纸投入有关
   Sales = b0 + b1*TV + b2*Radio + b3*Newspaper
"""
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics
import numpy as np
# from sklearn.cross_validation import train_test_split
import seaborn as sns  # 用于数据可视化的包，也属于matplotlib的内部包。


# （1）读取CSV文件 中的数据——利用pandas 读取
data = pd.read_csv('Advertising.csv', index_col=0)
# print(data.head())  # 数据前 5 行
# print(data.tail())  # 数据最后 5 项数据
# print(data.shape)   # 数据维度 （200，4）
# print(type(data))   # <class 'pandas.core.frame.DataFrame'>


# （2）分析数据——通过 matplotlib 或 seaborn 观察数据
# 使用散点图可视化特征与响应值之间的关系：x_vars为特征，y_vars 为观测值
sns.pairplot(data, x_vars=['TV','radio','newspaper'], y_vars='sales', height=5, aspect=0.8)
# plt.show()   # pairplot 为绘制x_vars每个维度与对应y_vars的散点图

# 增加 kind='reg' seaborn 可以添加一条最佳拟合直线和 95% 的置信带
# sns.pairplot(data, x_vars=['TV','radio','newspaper'], y_vars='sales', height=5, aspect=0.8, kind='reg')
# plt.show()


# （3） 建立线性回归模型完成预测
#  （3.1）使用pandas构建 X(特征xiangl)和 y(标签列)
# 创建特征列表
feature_cols = ['TV', 'radio', 'newspaper']
# X = data[feature_cols]
# 或 使用列表选择原始DataFrame的子集
X = data[['TV', 'radio', 'newspaper']]
# print(X.head())  # 输出前5项数据
# print(type(X))   # <class 'pandas.core.frame.DataFrame'>
# print(X.shape)   # (200, 3)

#从DataFrame中选择一个Series
y = data.sales   #或  y = data['sales']
# print(y.head())  # 输出前5项数据
# print(type(y))     # <class 'pandas.core.series.Series'>
# print(y.shape)     # (200,)

#  （3.2）构建训练集与测试集，分别保存在X_train，y_train，X_test，y_test
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1)
# print (X_train.shape)  # (150, 3) 即75%训练, 25%测试
# print (y_train.shape)  # (150,)
# print (X_test.shape)   # (50, 3)
# print (y_test.shape)   # (50,)

#  （3.3） sklearn实现线性回归：先导入相关线性回归模型，再做线性回归模拟
linreg = LinearRegression()
# .fit() 求得训练集X的均值啊，方差啊，最大值啊，最小值啊这些训练集X固有的属性。可以理解为一个训练过程
model = linreg.fit(X_train, y_train)

# print(model)   # LinearRegression(copy_X=True, fit_intercept=True, n_jobs=1, normalize=False)
print ('b0 = ', linreg.intercept_)    # b0 值
print ('[b1 b2 b3] = ', linreg.coef_) # [b1 b2 b3]
# print(linreg.coef_)       # [0.04656457 0.17915812 0.00345046]
# print(type(linreg.coef_)) # <class 'numpy.ndarray'>

print('得到的线性回归模型为：Sales = %f + %fTV + %fRadio + %fNewspaper'
      %(linreg.intercept_, linreg.coef_[0], linreg.coef_[1], linreg.coef_[2]))

# 将特征名称与系数对应
# print(zip(feature_cols, linreg.coef_))


#####-----至此，已得到线性回归模型：Sales = b0 + b1*TV + b2*Radio + b3*Newspaper ----###


#  （3.4） 预测：利用（3.2）步骤中的 X_test 进行预测。
y_pred = linreg.predict(X_test)  # 通过 predict 函数求预测结果
# print(y_pred)
# print(type(y_pred))  # <class 'numpy.ndarray'>

#  （3.5） 评价测度：计算预测结果的准确率
# 【均方根误差：RMSE】
print ('RMSE = ', np.sqrt(metrics.mean_squared_error(y_test, y_pred)))

####或采用以下方法求RMSE
# sum_mean = 0
# for i in range(len(y_pred)):
#     sum_mean += (y_pred[i] - y_test.values[i])**2
# RMSE = np.sqrt(sum_mean/50)
# print(RMSE)


#  【绘制曲线】
plt.figure()
plt.plot(range(len(y_pred)), y_pred, 'b', label="predict")
plt.plot(range(len(y_pred)), y_test, 'r', label="test")
plt.legend(loc="upper right")      #显示图中的标签
plt.xlabel("the number of sales")
plt.ylabel('value of sales')
plt.show()




#########  涉及知识点（1）：
# train_test_split
# 参数解释：
# train_data：被划分的样本特征集
# train_target：被划分的样本标签
# test_size：如果是浮点数，在0-1之间，表示样本占比；如果是整数的话就是样本的数量
# random_state：是随机数的种子。
# 随机数种子：其实就是该组随机数的编号，在需要重复试验的时候，保证得到一组一样的随机数。
# 比如你每次都填1，其他参数一样的情况下你得到的随机数组是一样的。但填0或不填，每次都会不一样。
# 随机数的产生取决于种子，随机数和种子之间的关系遵从以下两个规则：
# 种子不同，产生不同的随机数；种子相同，即使实例不同也产生相同的随机数。

#########  涉及知识点（2）：
# 最小二乘法线性回归：
# sklearn.linear_model.LinearRegression(fit_intercept=True, normalize=False,copy_X=True, n_jobs=1)
# 主要参数说明：
# fit_intercept：布尔型，默认为True，若参数值为True时，代表训练模型需要加一个截距项；
#                若参数为False时，代表模型无需加截距项。
# normalize：布尔型，默认为False，若fit_intercept参数设置False时，normalize参数无需设置；
#         若normalize设置为True时，则输入的样本数据将(X-X均值)/||X||；
#         若设置normalize=False时，在训练模型前，可以使用sklearn.preprocessing.StandardScaler进行标准化处理。
# 属性：
# coef_：回归系数(斜率)
# intercept_：截距项
# 主要方法：
# ①fit(X, y, sample_weight=None)
# ②predict(X)
# ③score(X, y, sample_weight=None)，
#         其结果等于1-(((y_true - y_pred) **2).sum() / ((y_true - y_true.mean()) ** 2).sum())



