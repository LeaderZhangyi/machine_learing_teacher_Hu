"""
功能：基于最小二乘法实现线性回归——————框架
"""

import numpy as np

class OLSLinearRegression:

    # _ols()方法：最小二乘法的实现，即 w估=（XTX）-1 XT y
    def _ols(self, X, y):
        # '''最小二乘法估算w'''
        # xTx = X.T * X
        # ws = xTx.I * X.T * y
        # return ws 

        # -------老师答案---------
    
        # tmp = np.linalg.inv(np.matmul(X.T, X))
        # tmp = np.matmul(tmp, X.T)
        # return np.matmul(tmp, y)
        # 或：
        return np.linalg.inv(X.T.dot(X)).dot(X.T).dot(y)
        # return np.linalg.inv(X.T @ X) @ X.T @ y

    # _preprocess_data_X()方法：对X进行预处理，添加x0列，并设置为1
    def _preprocess_data_X(self, X):
        '''数据预处理'''
        # new_X = np.c_[np.ones((X.shape[0],1))]
        # #（添加 扩展X, 添加x0列并置1. 的代码）
        # return new_X

        # ----------------老师答案---------

        # 扩展X, 添加x0列并置1.
        m, n = X.shape
        X_ = np.empty((m, n + 1))
        X_[:, 0] = 1
        X_[:, 1:] = X
        return X_

    # train()方法：训练模型，调用_ols()方法估算模型参数w，并保存
    def train(self, X_train, y_train):
        '''训练模型'''
        X_train = self._preprocess_data_X(X_train)
        #（添加 预处理X_train(添加x0=1)的代码）
        self.w = self._ols(X_train,y_train)
        


    # predict()方法：预测，实现函数 hw(x)=wTw，对x中每个实例进行预测
    def predict(self, X):
        '''预测'''
        # hwx = self.w.T * X 
        # return hwx

        # -----------老师答案-----------
        X = self._preprocess_data_X(X)
        return np.matmul(X, self.w)

