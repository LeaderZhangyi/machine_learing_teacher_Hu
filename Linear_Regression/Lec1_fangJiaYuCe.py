# -*- coding: utf-8 -*-
"""
《《《一元线性回归问题》》》
用Python实现一元线性回归：房价预测问题——设y(x)=a+bx
使用数据集： input_fangjia.csv

"""


import matplotlib.pyplot as plt
import matplotlib
import numpy as np  
import pandas as pd  
from sklearn import datasets, linear_model


'''读取数据'''
def get_data(file_name):
    data = pd.read_csv(file_name)  #用pandas 读取cvs 文件.
    X_parameter = []  # square_feet
    Y_parameter = []  # price
    for single_square_feet ,single_price_value in zip(data['square_feet'],data['price']):#遍历数据，
        X_parameter.append([float(single_square_feet)])#存储在相应的list列表中
        Y_parameter.append(float(single_price_value))
    return X_parameter,Y_parameter


# Function for Fitting our data to Linear model将数据拟合到线性回归模型
'''将数据拟合到线性回归模型'''
def linear_model_main(X_parameters,Y_parameters,predict_value):
    # Create linear regression object
    regr = linear_model.LinearRegression() #创建线性回归对象
    # 创建线性模型
    regr.fit(X_parameters, Y_parameters)   #训练模型
    predict_outcome = regr.predict(predict_value)
    # 创建字典 predictions，存放a、b、预测值。
    predictions = {}
    predictions['intercept'] = regr.intercept_    # 截距：Y = ax + b 中的 b 值
    predictions['coefficient'] = regr.coef_       # Y = ax + b 中的 a 值
    predictions['predicted_value'] = predict_outcome
    return predictions


if __name__ == '__main__':
    # 1. 读取数据集
    file_name = 'input_fangjia.csv'
    X, Y = get_data(file_name)

    #2. 模型训练，并预测
    predict_value = np.array((4,))  # 要预测的 square_feet 值
    result = linear_model_main(X, Y, predict_value.reshape(1, -1))
    print("Intercept value ", result['intercept'])  # Y = ax + b 中的 b 值
    print("coefficient", result['coefficient'])     # Y = ax + b 中的 a 值
    print('得到的线性回归模型为：Y = %fx + %f'%(result['coefficient'], result['intercept']))
    print("Predicted value: ", result['predicted_value'])  # 预测值





"""#######以上房价预测代码完成########"""



"""#######以下为了验证数据是否拟合线性回归#########"""
# Function to show the resutls of linear fit model
def show_linear_line(X_parameters,Y_parameters):
# Create linear regression object
    matplotlib.rcParams['font.family'] = 'STSong'
    regr = linear_model.LinearRegression()
    regr.fit(X_parameters, Y_parameters)
    plt.scatter(X_parameters,Y_parameters,color='blue')
    plt.plot(X_parameters,regr.predict(X_parameters),color='red',linewidth=4)
    plt.xlabel('房间面积')
    plt.ylabel('价格/每平方英尺')
    # plt.xticks(())
    # plt.yticks(())
    plt.show()


# if __name__ == '__main__':
#     # 1. 读取数据集
#     file_name = 'input_fangjia.csv'
#     X, Y = get_data(file_name)
#     show_linear_line(X, Y)

