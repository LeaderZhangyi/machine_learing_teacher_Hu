"""
红酒口感预测
"""

import numpy as np
# import linear_regression
from linear_regression import OLSLinearRegression  # 本地文件定义的类
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

# （1）准备数据
data = np.genfromtxt('data_winequality.csv', delimiter=';', skip_header=True)
X = data[:, :-1]
y = data[:, -1]
# print(data.shape) # (1599, 12)
# print(type(X))    # <class 'numpy.ndarray'>  (1599, 11)
# print(type(y))    # <class 'numpy.ndarray'>  (1599,)


# （2）模型训练与测试：采用 OLS 最小二乘法
# （2.1）创建模型
ols_lr = OLSLinearRegression()
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)
# （2.2）训练模型
ols_lr.train(X_train, y_train)
# （2.3）使用训练好的模型，对测试集进行预测
y_pred = ols_lr.predict(X_test)
# print(y_pred)
# （2.4）使用MSE(均方误差)衡量回归模型的性能
mse = mean_squared_error(y_test, y_pred)
print('mse：', mse)   # 模型在测试集上MSE
# 或计算测试模型在训练集上的MSE
y_train_pred = ols_lr.predict(X_train)
mse_train = mean_squared_error(y_train, y_train_pred)
print('mse_train：', mse_train)




'''知识点：
# （1）genfromtxt函数
# - genfromtxt函数创建数组表格数据
# - genfromtxt主要执行两个循环运算。第一个循环将文件的每一行转换成字符串序列。
#             第二个循环将每个字符串序列转换为相应的数据类型。
# - genfromtxt能够考虑缺失的数据,但其他更快和更简单的函数像loadtxt不能考虑缺失值。
# - 使用前需导入相应模块
# - skip_header和skip_footer参数
# 一个文件的页眉会阻碍文件的处理。在这种情况下,我们需要使用skip_header可选参数。这个参数
    的值必须是一个整数,跳过文件开头的对应的行数,然后再执行任何其他操作。同样的,我们通过使用
    skip_footer属性和n的值可以跳过文件的最后n行。默认值都为0.
# 参考链接： https://blog.csdn.net/chang_yuan_2011/article/details/21300443

# （2）train_test_split中 test_size 为测试集占比，如果为整数就代表
# 样本的数量，random_state 为随机数种子

'''

