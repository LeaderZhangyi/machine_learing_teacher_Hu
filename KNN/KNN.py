from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def get_dis(x, y):
    return np.sqrt(np.sum(np.square(x - y)))


def CreateData():
    group = np.array([[1, 1.1], [1, 1], [0, 0], [0, 0.1]])
    labels = ['A', 'A', 'B', 'B']
    return group, labels


def Classify(group, labels, k, data):
    group['label'] = labels
    df = group.copy()
    tmpxy = df[['x', 'y']].values
    dis = [get_dis(data, item) for item in tmpxy]
    df['distance'] = dis
    df = df.sort_values(by='distance').head(k)
    return max(df['label'].value_counts(normalize=True).index)


def to_check(group, labels, data, k):
    group['label'] = labels
    df = group.copy()
    # x_train, x_test, y_train, y_test = train_test_split(df[['x', 'y']].values, df['label'].values, test_size=0.25)
    knn = KNeighborsClassifier(n_neighbors=k)
    #     # knn.fit(group['x'], group['y'])
    knn.fit(df[['x', 'y']].values, df['label'].values)
    print("sklearn里面的knn的预测值为：", knn.predict(data))


if __name__ == '__main__':
    group, labels = CreateData()
    group = pd.DataFrame(group, columns=['x', 'y'])
    op = [0 if i == 'A' else 1 for i in labels]
    colors = ["blue", "red"]
    color = [colors[i] for i in op]
    plt.scatter(group['x'], group['y'], c=color)
    plt.scatter(0, 0.3, c='g', marker='*')
    plt.show()
    res = Classify(group, labels, 3, np.array([0, 0.3]))
    col = "blue"
    if res == 'B':
        col = 'red'
    plt.scatter(group['x'], group['y'], c=color)
    plt.scatter(0, 0.3, c=col)
    plt.show()
    print("手写出来的预测值为：",res)
    print("分割线".center(100,'='))
    # 和sklearn里面的knn的api进行相应的对比
    to_check(group, labels, np.array([[0, 0.3]]), 3)
