import numpy as np

"""
KNN算法实现：
    1. 计算已知类别数据集中的点与当前点的距离
    2. 按照距离依次排序
    3. 选取与当前点距离最小的K个点
    4. 确定前k个点所在类别的出现概率
    5. 返回前k个点出现频率最高的类别作为当前点预测分类
"""

# def classify_knn(inX,dataSet,labels,k):
dataSet = np.array([[1, 2], [2, 2], [5, 4], [6, 3]])
labels = ['A', 'A', 'B', 'B']
dataSetSize = dataSet.shape[0]

# 1. 计算距离
diffMat = np.tile([5, 2], (dataSetSize, 1)) - dataSet
# print(diffMat)
sqdistance = diffMat ** 2
distance = sqdistance.sum(axis=1) ** 0.5

# 2. 排序
sort_distance_index = np.argsort(distance)

# 3. 选取与当前点距离最小的k = 3个点
classCount = {}
for i in range(3):
    vote_label = labels[sort_distance_index[i]]
    classCount[vote_label] = classCount.get(vote_label, 0) + 1  # get(xx,default = ??)

# 4. 确定概率
sort_count_class = sorted(classCount.items(), key=lambda x: x[1], reverse=True)
print(sort_count_class)

# 5. 返回
print(sort_count_class[0][0])
